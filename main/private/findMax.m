function [x,f] = findMax(fn,x0,maxit,tol,use_hess,disp_level)
%   findMax:
%       Compute a local maximizer of fn using x0 as the starting point. 
%       fminunc is used.  
% 
%   INPUT:
%       fn                          [function handle]
%           [f,g,h] = fn(x) returns function value, gradient, and Hessian
% 
%       x0                          [real valued column vector]
%           Starting point for optimizaton
%
%       maxit                       [positive integer]
%           Maximum number of iterations for fminunc
%
%       tol                         [positive real scalar]
%           Convergence tolerance for fminunc
% 
%       use_hess                    [logical]
%           False: fn only returns gradient --- h should be returned as nan
%           True: fn returns gradient and Hessian
%           Providing the Hessian increases local convergence from
%           superlinear to quadratic.  
%       
%       disp_level                  [0, 1, or 2]
%           0: 'off'
%           1: 'iter'
%           2: 'iter-detailed'
%   
%   OUTPUT:
%       x                           [real valued column vector]
%           computed maximizer
%
%       f                           [real value]
%           computed maximal value of fn, i.e., f = fn(x)
%
%
%   For comments/bug reports, please visit the ROSTAPACK GitLab webpage:
%   https://gitlab.com/timmitchell/ROSTAPACK
%
%   findMax.m introduced in ROSTAPACK Version 3.0
%
% =========================================================================
% |  ROSTAPACK: RObust STAbility PACKage                                  |
% |  Copyright (C) 2014-2019 Tim Mitchell                                 |
% |                                                                       |
% |  This file is part of ROSTAPACK                                       |
% |                                                                       |
% |  ROSTAPACK is free software: you can redistribute it and/or modify    |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  ROSTAPACK is distributed in the hope that it will be useful,         |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/agpl.html>.                             |
% =========================================================================

       
    opts    = getOptions(maxit,tol,use_hess,disp_level);

    [x,f]   = fminunc(@minFn,x0,opts);
    f       = -f;

    function [f,g,h] = minFn(x)
        [f,g,h] = fn(x);
        [f,g,h] = deal(-f,-g,-h);
    end
end

function opts = getOptions(maxit,tol,use_hess,disp_level)
    
    if use_hess
        hess_opts = {'HessianFcn','objective'};  
    else
        hess_opts = {};
    end
    
    switch disp_level
        case 0
            d = 'off';
        case 1
            d = 'iter';
        case 2
            d = 'iter-detailed';
        otherwise
            error('disp_level must be 0, 1, or 2');
    end
    disp = {'Display',d};

    opts = optimoptions(                                    ...
                'fminunc',                                  ...
                'Algorithm',                'trust-region', ...
                'SpecifyObjectiveGradient', true,           ...
                hess_opts{:},                               ...
                'MaxIterations',            maxit,          ...
                'OptimalityTolerance',      tol,            ...
                'StepTolerance',            tol,            ...
                'FunctionTolerance',        tol,            ...
                disp{:}                                     );
  
end
