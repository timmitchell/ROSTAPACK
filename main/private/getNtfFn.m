function [ntfFn,bestFn,dataFn] = getNtfFn(A,B,C,D,E,is_real,opts)
%   getNtfFn:
%       Builds a function handle for evaluating the norm of the transfer
%       function at different frequencies along the imaginary axis or unit
%       circle. 
% 
%   INPUT:
%       A,B,C,D,E                   [matrices]
%           System matrices.  A must be provided while B,C,D,E can be given
%           explicitly or as [] for their shortcuts.
% 
%       is_real                     [logical]
%           When true, the norm of the transfer function is considered 
%           symmetric, i.e. ntfFn(w) == ntfFn(-w).
%
%       opts                        [struct]
%       .discrete_time              [logical]
%           False:  frequencies along the imaginary axis
%           True:   frequencies on the unit circle
%
%       .order                      [0, 1 or 2]
%           0:      ntfFn returns the norm of the transfer function value
%           1:      ntfFn also returns the first derivative
%           2:      ntfFn also returns first and second derivatives
%
%       .solve_type                 [0,1,2]
%           Sets how inverses of zE-A should be applied:
%           0: via upper triangular Hessenberg factorization 
%           1: via LU
%           2: via LU with permutations 
% 
%       .force_two_norm             [logical]
%           When B=C=I, D=0, and n=m=p, force ||(zE-A)^{-1}|| to be used 
%           instead of the reciprocal of the smallest singular value of
%           zE-A.  (Since GESDD in LAPACK can have numerical difficulty for
%           very poorly scaled matrices.)
%   
%   OUTPUT:
%       ntfFn                       [function handle]
%           where [f,g,h] = ntfFn(w) returns
%               f:  the norm of the transfer function at frequency w
%               g:  the first derivative with respect to w
%               h:  the second derivative with respect to w 
%                   (nans for g and h when not requested)
%
%       bestFn                      [function handle]
%           where [w,f,g,h] = bestFn() returns 
%               w:  frequency of those evaluated that yields the largest 
%                   value of the norm of the transfer function encountered
%               f:  value of the norm of the transfer function at w
%               g:  first derivative at w
%               h:  second derivative at w
%                   (nans for g and h when not requested)
% 
%       dataFn                      [function handle]
%           where [u,v,n_evals] = dataFn() additionally returns 
%               u:  left singular vector of transfer function matrix at w 
%               v:  right singular vector of transfer function matrix at w 
%               n_evals: total number of function evalations incurred
%
%   See also makeNtfFns and makeFrequencyCache.
%
%
%   For comments/bug reports, please visit the ROSTAPACK GitLab webpage:
%   https://gitlab.com/timmitchell/ROSTAPACK
%
%   getNtfFn.m introduced in ROSTAPACK Version 3.0
%
% =========================================================================
% |  ROSTAPACK: RObust STAbility PACKage                                  |
% |  Copyright (C) 2014-2019 Tim Mitchell                                 |
% |                                                                       |
% |  This file is part of ROSTAPACK                                       |
% |                                                                       |
% |  ROSTAPACK is free software: you can redistribute it and/or modify    |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  ROSTAPACK is distributed in the hope that it will be useful,         |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/agpl.html>.                             |
% =========================================================================
   
    if opts.discrete_time
        eta     = 1;
    else
        eta     = 0;
    end
    
    cacher          = makeFunctionCache(is_real);
    [fn0,fn1,fn2]   = makeNtfFns(A,B,C,D,E,opts);
   
    switch opts.order
        case 0 
            fn      = fn0;
        case 1
            fn      = fn1;
        case 2
            fn      = fn2;
        otherwise
            error('opts.order must be 0, 1, or 2');
    end
   
    ntfFn       = @ntf;
    bestFn      = @getBest;
    dataFn      = @getData;
    
    w_max       = 0;
    f_max       = -inf;
    df_max      = 0;
    ddf_max     = 0;
    u_max       = [];
    v_max       = [];
    count       = 0;
   
    function [f,g,h] = ntf(w)
        [f,g,h]     = cacher.get(w);
        if ~isempty(f)
            return
        end
        
        [f,g,h,u,v] = fn(eta,w,2);
        cacher.add(w,f,g,h);
           
        if f > f_max
            w_max   = w;
            f_max   = f;
            df_max  = g;
            ddf_max = h;
            u_max   = u;
            v_max   = v;
        end
        count       = count + 1;
    end

    function [w,f,df,ddf] = getBest()
        w       = w_max;
        f       = f_max;
        df      = df_max;
        ddf     = ddf_max;
    end

    function [u,v,n_evals] = getData()
        u       = u_max;
        v       = v_max;
        n_evals = count;
    end
end