function cacher = makeFunctionCache(is_symmetric)
%   makeFunctionCache:
%       Builds a caching object for storing function values and first and
%       second derivatives based on the real scalar function input x.
%
%   INPUT:
%       is_symmetric                [logical]
%           False:  values x and -x are different keys
%           True:   values x and -x should be treated as the same key,
%                   i.e., fn(x) = fn(-x) for some function fn, but the 
%                   signs of the first and second derivatives are opposite
%                   at x and -x.
%   
%   OUTPUT:
%       cacher                      [struct of function handles]
%      .add                         [function handle]
%           cacher.add(x,f,g,h) adds real scalars f, g, and h under the key
%           x.  If x (or -x if is_symmetric is true) is already in the
%           cache, nothing is done.  If the cache is currently full, its
%           size will be doubled to accommodate adding the new entry.
%
% 
%       .get                        [function handle]
%           [f,g,h] = cacher.get(x) returns the stored f,g,h values if x
%           (or -x if is_symmetric is true) is in the cache.  Otherwise,
%           []'s are return for f, g, and h. not in the cache
%
%
%   For comments/bug reports, please visit the ROSTAPACK GitLab webpage:
%   https://gitlab.com/timmitchell/ROSTAPACK
%
%   makeFunctionCache.m introduced in ROSTAPACK Version 3.0
%
% =========================================================================
% |  ROSTAPACK: RObust STAbility PACKage                                  |
% |  Copyright (C) 2014-2021 Tim Mitchell                                 |
% |                                                                       |
% |  This file is part of ROSTAPACK                                       |
% |                                                                       |
% |  ROSTAPACK is free software: you can redistribute it and/or modify    |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  ROSTAPACK is distributed in the hope that it will be useful,         |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/agpl.html>.                             |
% =========================================================================

    n           = 0;
    cache       = zeros(4,100);
    
    cacher.get  = @get;
    cacher.add  = @add;
    
    function [f,df,ddf] = get(x)
        idx = getIndex(x);
        if isempty(idx)
            [f,df,ddf] = deal([]);   
            return
        end        
        f       = cache(2,idx);
        df      = cache(3,idx);
        ddf     = cache(4,idx);
        if is_symmetric && x < 0
            df  = -df;
            ddf = -ddf;
        end
    end

    function add(x,f,df,ddf)
        idx = getIndex(x);
        if ~isempty(idx)
            return
        end
        if is_symmetric && x < 0
            x   = -x;
            df  = -df;
            ddf = -ddf;
        end
        n = n + 1;
        k = size(cache,2);
        if n > k
            cache   = [cache zeros(4,k)];
        end
        cache(:,n)  = [x; f; df; ddf];
    end
    
    function idx = getIndex(x)
        x_cache = cache(1,1:n);
        if is_symmetric 
            x   = abs(x);
        end
        [diff,idx] = min(abs(x_cache - x));    
        if diff > 0
            idx = [];
        end
    end

end