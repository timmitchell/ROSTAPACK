function [f_max,w_max,info] = normTfPeak(A,varargin)
%   normTfPeak:
%       Given a linear dynamical system, in state-space matrix form,
%
%           E xdot = Ax + Bu
%                  = Cx + Du,
%
%       or its discrete-time analogue, normTfPeak finds a peak of the norm
%       of the transfer function along the imaginary axis or unit circle as
%       appropriate.  It generally converges to local maximizers which are
%       often global maximizers, but this is not guaranteed.
%
%       Computationally, by default normTfPeak does an O(n^3) work
%       factorization and then does O(n^2) work to evaluate the norm of the
%       transfer function and derivatives when running local optimization.
%       For large n, normTfPeak can be configured to skip the O(n^3) work
%       factorization and instead do (sparse) LU, in which case, evaluating
%       the norm of the transfer function is generally O(n) work.  The
%       underlying algorithm has a quadratic rate of convergence.
% 
%   USAGE:
%       Special case of B=C=E=I and D=0:
%       [f_max,w_max,info] = normTfPeak(A)
%       [f_max,w_max,info] = normTfPeak(A,opts)
%
%       General systems:
%       [f_max,w_max,info] = normTfPeak(A,B,C,D,E)
%       [f_max,w_max,info] = normTfPeak(A,B,C,D,E,opts)
%
%   INPUT:
%       System matrix A         [required]
%           Matrix A must be provided as an explicit dense or sparse matrix
%       
%       System matrices B,C,D,E [optional]
%           Matrices B,C,D,E must be provided as explicit dense or sparse
%           matrices.  Setting any to [] acts as a shortcut for their
%           corresponding default values:
%           B: [] indicates B is a n by m (sparse) identity.
%           C: [] indicates C is a p by n (sparse) identity.
%           D: [] indicates D is the zero matrix.
%           E: [] indicates E is a n by n (sparse) identity.
%           If B and D are both [], m is automatically set to n.
%           If C and D are both [], p is automatically set to n.
%   
%       opts                    [optional: struct of parameters]
%           An optional struct of settable parameters or [].
%           To see available parameters and their descriptions, type:
%           >> help normTfPeakOptions
%                 
%   OUTPUT:
%       f_max      
%           The highest value of the norm of the transfer function
%           encountered along the imaginary axis or unit circle, as
%           appropriate.
%
%       w_max
%           The frequency that attains the highest peak found.  
%
%       info
%           Struct containing metadata about the computation.
%   
%       .fn_evals
%           Number of evaluations of the norm of the transfer function.
%
%       .u 
%           Left singular vector of the transfer function evaluated at
%           frequency w_max.
%
%       .v 
%           Right singular vector of the transfer function evaluated at
%           frequency w_max.
%
%   See also normTfPeakOptions.
%
%
%   For comments/bug reports, please visit the ROSTAPACK GitLab webpage:
%   https://gitlab.com/timmitchell/ROSTAPACK
%
%   normTfPeak.m introduced in ROSTAPACK Version 3.0.
%
% =========================================================================
% |  ROSTAPACK: RObust STAbility PACKage                                  |
% |  Copyright (C) 2014-2021 Tim Mitchell                                 |
% |                                                                       |
% |  This file is part of ROSTAPACK                                       |
% |                                                                       |
% |  ROSTAPACK is free software: you can redistribute it and/or modify    |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  ROSTAPACK is distributed in the hope that it will be useful,         |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/agpl.html>.                             |
% =========================================================================

    % Check all input arguments
    err_id = 'normTfPeak:invalidInputs';
    switch nargin 
        case {1,2}  
            % Only A matrix so set other matrices to []
            [B,C,D,E]   = deal([]);
            if nargin == 1
                opts    = [];
            else
                opts    = varargin{1};
            end
        case {5,6}  
            % A,B,C,D,E provided
            [B,C,D,E]   = deal(varargin{1:4});
            if nargin == 5
                opts    = [];
            else
                opts    = varargin{5};
            end
        otherwise
            error(err_id,'invalid number of input arguments');
    end
    % B,C,E will be replaced by [] if they are explicit identities.
    % D will be replaced with either [] or sparse(p,m) if it is zero.
    [A,B,C,D,E]     = processSystemMatrices(A,B,C,D,E);
    assert(isempty(opts) || isstruct(opts),err_id,'invalid opts argument');
            
    opts            = normTfPeakOptions(opts);

    discrete        = opts.discrete_time;
    tol             = opts.tol;
    use_hessian     = opts.use_hessian;
    plotting        = opts.plotting;
    suppress_warns  = opts.suppress_warnings;
  
    % If the problem is symmetric, we can restrict all searches to
    % nonnegative frequencies.  Checks if all matrices are real and
    % converts any to bonafide real-valued matrix structures if any have an
    % imaginary part that is physically present but nonetheless zero.
    [is_real,A,B,C,D,E]         = isRealSystem(A,B,C,D,E);
    
    % The internal computations, e.g., the LUs and backsolves used in the
    % norm of transfer function evaluations, can produce warnings,
    % particularly when done near eigenvalues.  However, as this code is
    % designed to cope with causes of these warnings, there is typically no
    % need to see them.  As such, by default, we disable all warnings and
    % then restore the warning state after the computation is finished.
    if suppress_warns
        w_state     = warning();
        warning('off');
    end
     
    opts.order  = 1 + use_hessian;
    [ntfFn,getBestFn,getDataFn] = getNtfFn(A,B,C,D,E,is_real,opts);
    % Calculate the gains for some initial guesses of the frequency
    w0          = getStartingGuesses();
    f0          = arrayfun(ntfFn,w0);
    
    if plotting
        plot(w0,f0,'rx','LineWidth',2);
    end
    
    disp_level  = 0;
    runOpt      = @(x0) findMax(ntfFn,x0,50,tol,use_hessian,disp_level);
    
    % This cannot be converted to a parfor loop as is because ntfFn has a
    % shared cache hidden inside it.  But one can easily just create a
    % separate ntfFn for each runOpt call if one wishes to do optimization
    % in parallel.
    [w,f]       = arrayfun(runOpt,w0);
        
    if plotting
        plot(w,f,'ro','LineWidth',2);
    end
         
    % Get highest peak found 
    [w_max,f_max]   = getBestFn(); 
    [u,v,fn_evals]  = getDataFn();
    if discrete
        w_max       = wrapToPi(w_max);
    elseif ~isempty(D)
        [U,S,V]     = svd(full(D));
        normD       = abs(S(1));
        if normD > f_max
            f_max   = normD;
            w_max   = inf;
            u       = U(:,1);
            v       = V(:,1);
        end
    end
    info            = makeInfo(fn_evals,u,v);
    if is_real
        w_max       = abs(w_max);
    end
    
    if suppress_warns
        warning(w_state);
    end
    
    % Private functions
 
    function w0 = getStartingGuesses()
        
        % Get any frequency guesses from the user, ensure as a column
        w0 = opts.w0(:);
        
        if isempty(w0)
            if discrete        
                w0  = [0; pi];
            else
                w0  = 0;
            end
            return
        end
          
        if is_real
            w0      = abs(w0);
        end
        w0          = unique(w0);
    end

end

function info = makeInfo(fn_evals,u,v)
    info = struct(  'fn_evals', fn_evals,   ...
                    'u',        u,          ...
                    'v',        v           );
end