function [f_max,w_max,info] = normTfMaxPeak(A,varargin)
%   normTfMaxPeak:
%       Given a linear dynamical system, in state-space matrix form,
%
%           E xdot = Ax + Bu
%                  = Cx + Du,
%
%       or its discrete-time analogue, normTfMaxPeak computes its
%       H-infinity or L-infinity norm.  E should be invertible.  The
%       default behavior is to compute the H-infinity norm of a
%       continuous-time system, but this can be changed to the L-infinity
%       norm by setting opts.linf_norm=true.
%       
%       Computationally, normTfMaxPeak requires O(n^3) work and O(n^2)
%       memory per iteration.  Every iteration, a dense matrix pencil of
%       order 2n is formed and all of its eigenvalues are computed.  The
%       underlying algorithm has a quadratic rate of convergence.
% 
%   USAGE:
%       Special case of B=C=E=I and D=0:
%       [f_max,w_max,info] = normTfMaxPeak(A)
%       [f_max,w_max,info] = normTfMaxPeak(A,opts)
%
%       General systems:
%       [f_max,w_max,info] = normTfMaxPeak(A,B,C,D,E)
%       [f_max,w_max,info] = normTfMaxPeak(A,B,C,D,E,opts)
%
%   INPUT:
%       System matrix A         [required]
%           Matrix A must be provided as an explicit dense or sparse matrix
%       
%       System matrices B,C,D,E [optional]
%           Matrices B,C,D,E must be provided as explicit dense or sparse
%           matrices.  Setting any to [] acts as a shortcut for their
%           corresponding default values:
%           B: [] indicates B is a n by m (sparse) identity.
%           C: [] indicates C is a p by n (sparse) identity.
%           D: [] indicates D is the zero matrix.
%           E: [] indicates E is a n by n (sparse) identity.
%           If B and D are both [], m is automatically set to n.
%           If C and D are both [], p is automatically set to n.
%   
%       opts                    [optional: struct of parameters]
%           An optional struct of settable parameters or [].
%           To see available parameters and their descriptions, type:
%           >> help normTfMaxPeakOptions
%                 
%   OUTPUT:
%       f_max      
%           The computed value of the H-infinity or L-infinity norm.
%
%       w_max
%           The frequency that attains the H-infinity or L-infinity norm.
%
%       info
%           Struct containing metadata about the computation.
%       
%       .converged
%           True:   Converged to the desired relative accuracy
%           False:  Did not converge to the desired relative accuracy.  
%                   This likely means that opts.maxit needs to be
%                   increased.
%
%       .iters
%           Number of iterations incurred before halting, which is equal to
%           the number of order 2n eigenvalue problems solved.
%   
%       .fn_evals
%           Number of evaluations of the norm of the transfer function.
%
%       .u 
%           Left singular vector of the transfer function evaluated at
%           frequency w_max.
%
%       .v 
%           Right singular vector of the transfer function evaluated at
%           frequency w_max.
%
%   See also normTfMaxPeakOptions.
%
%
%   For comments/bug reports, please visit the ROSTAPACK GitLab webpage:
%   https://gitlab.com/timmitchell/ROSTAPACK
%
%   normTfMaxPeak.m introduced in ROSTAPACK Version 3.0.
%
% =========================================================================
% |  ROSTAPACK: RObust STAbility PACKage                                  |
% |  Copyright (C) 2014-2021 Tim Mitchell                                 |
% |                                                                       |
% |  This file is part of ROSTAPACK                                       |
% |                                                                       |
% |  ROSTAPACK is free software: you can redistribute it and/or modify    |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  ROSTAPACK is distributed in the hope that it will be useful,         |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/agpl.html>.                             |
% =========================================================================

    % Check all input arguments
    err_id = 'normTfMaxPeak:invalidInputs';
    switch nargin 
        case {1,2}  
            % Only A matrix so set other matrices to []
            [B,C,D,E]   = deal([]);
            if nargin == 1
                opts    = [];
            else
                opts    = varargin{1};
            end
        case {5,6}  
            % A,B,C,D,E provided
            [B,C,D,E]   = deal(varargin{1:4});
            if nargin == 5
                opts    = [];
            else
                opts    = varargin{5};
            end
        otherwise
            error(err_id,'invalid number of input arguments');
    end
    % B,C,E will be replaced by [] if they are explicit identities.
    % D will be replaced with either [] or sparse(p,m) if it is zero.
    [A,B,C,D,E]     = processSystemMatrices(A,B,C,D,E);
    assert(isempty(opts) || isstruct(opts),err_id,'invalid opts argument');
            
    opts            = normTfMaxPeakOptions(opts);
    spec_opts       = getSpectrumOptions(opts);
    
    discrete        = opts.discrete_time;
    linf_norm       = opts.linf_norm;
    maxit           = opts.maxit;
    tol             = opts.tol;
    
    opt_tol         = tol;%/10;
    opt_calls       = opts.optimization_calls;
    use_hessian     = opt_calls > 0 && opts.use_hessian;
    eig_tol         = opts.eig_tol;
    suppress_warns  = opts.suppress_warnings;
    plotting        = opts.plotting;
        
    % If the problem is symmetric, we can restrict all searches to
    % nonnegative frequencies.  Checks if all matrices are real and
    % converts any to bonafide real-valued matrix structures if any have an
    % imaginary part that is physically present but nonetheless zero.
    [is_real,A,B,C,D,E]         = isRealSystem(A,B,C,D,E);
    
    % Compute spectrum to check stability and get initial guesses   
    [~,~,d_co,~,inf_co,inf_u]   = getEigenvalues(A,B,C,D,E,spec_opts);
    if inf_co || inf_u
        error(err_id,'E must be invertible');
    end  
    [is_stable,d_stab]          = isStable(d_co);
    if ~is_stable
        f_max       = inf;
        w_max       = inf;
        info        = makeInfo(true,0,0,[],[]);
        return
    end
    
    % The internal computations, e.g., the LUs and backsolves used in the
    % norm of transfer function evaluations, can produce warnings,
    % particularly when done near eigenvalues.  However, as this code is
    % designed to cope with causes of these warnings, there is typically no
    % need to see them.  As such, by default, we disable all warnings and
    % then restore the warning state after the computation is finished.
    if suppress_warns
        w_state     = warning();
        warning('off');
    end
     
    opts.order      = (opt_calls > 0)*(1 + use_hessian);
    [ntfFn,getBestFn,getDataFn] = getNtfFn(A,B,C,D,E,is_real,opts);
    % Calculate the gains for some initial guesses of the frequency
    [w0,w_user]     = getStartingGuesses();
    [f0,df0,ddf0]   = arrayfun(ntfFn,w0);
    
    if plotting
        plotInitialPoints(w0,f0,w_user);
    end
    
    % Optionally start local optimization from these points
    if opt_calls > 0  
        disp_level = 0;
        runOpt = @(x0) findMax(ntfFn,x0,50,opt_tol,use_hessian,disp_level);
        findMaxes(cell(size(w0)),w0,f0,df0,ddf0);
    end
      
    % Best estimate so far
    [w_max,f_max,df_max]    = getBestFn();
    converged               = false;
    
    for iter = 1:maxit
        [intervals,ws,fs,dfs,ddfs] = computeIntervals(w_max,f_max,df_max);
        
        % If there are no intervals, we can quit
        if isempty(intervals)
            converged = true;
            break
        end
            
        if opt_calls > 0 
            findMaxes(intervals,ws,fs,dfs,ddfs);
        end
        
        f_prev                  = f_max;
        [w_max,f_max,df_max]    = getBestFn();
       % fprintf('w = %.15e f = %.15e\n',w_max,f_max);
        if f_max <= f_prev
            converged           = true;
            break
        end
    end
    
    [u,v,fn_evals]  = getDataFn();
    if discrete
        w_max       = wrapToPi(w_max);
    elseif ~isempty(D)
        [U,S,V]     = svd(full(D));
        normD       = abs(S(1));
        if normD > f_max
            f_max   = normD;
            w_max   = inf;
            u       = U(:,1);
            v       = V(:,1);
        end
    end
    info            = makeInfo(converged,iter,fn_evals,u,v);
    if is_real
        w_max       = abs(w_max);
    end
    
    if suppress_warns
        warning(w_state);
    end
    
    % Private functions
    
    function [ints,w,f,df,ddf] = computeIntervals(w_prev,f_prev,df_prev)
        
        f_pert      = f_max*(1 + tol);
        eigSolver   = makeEigSolver(A,B,C,D,E,1/f_pert,discrete);
        [eHSP,f_MN] = eigSolver(0 + discrete);
        ints        = formAllIntervals(eHSP,getIntervalOpts(f_MN));
        if isempty(ints)
            w       = [];
            f       = [];
            df      = [];
            ddf     = [];
            return
        end

        w                   = cellfun(@mean,ints);
        if is_real
            % For symmetric problems, intervals in the bottom half-plane
            % can be discarded.  However, we can also discard intervals 
            % with midpoints equal to w = 0 and w = pi.  This is because
            % w = 0 and w = pi must be local minima or maxima so any 
            % subsequent intervals with these midpoints cannot be under the
            % norm of the transfer function.  This is in contrast to the
            % spectral value set abscissa/radius case, where such intervals
            % may be cross sections at any iteration (due to using local
            % root finding).
            indx            = w <= 0;
            w(indx)         = [];
            ints(indx)      = [];
            if discrete
                indx        = w >= pi;
                w(indx)     = [];
                ints(indx)  = [];
            end
            if isempty(ints)
                w       = [];
                f       = [];
                df      = [];
                ddf     = [];
                return
            end
        end
        
        f_max               = f_pert;
        [f,df,ddf,rd]       = evaluateFreqs(w);
        [max_rd,idx]        = max(rd);
        if abs(max_rd) < 1e-14
            [ints,w,f,df,ddf]   = splitAt(ints,w,f,df,ddf,idx);
        end
        
        idx                 = f > f_max;
        w                   = w(idx);
        f                   = f(idx);
        df                  = df(idx);
        ddf                 = ddf(idx);
        ints                = ints(idx);

        if plotting
            plotIntervals(ints,w,f);
        end
    end

    function [ints,w,f,df,ddf] = splitAt(ints,w,f,df,ddf,idx)
        to_split        = ints{idx};
        w1              = to_split(1);
        w2              = to_split(2);
        int1            = [w1 w(idx)];
        int2            = [w(idx) w2];
        wn              = [mean(int1); mean(int2)];
        [fn,dfn,ddfn]   = evaluateFreqs(wn);
        w               = replaceAt(w,wn,idx);
        f               = replaceAt(f,fn,idx);
        df              = replaceAt(df,dfn,idx);
        ddf             = replaceAt(ddf,ddfn,idx);
        ints            = replaceAt(ints,{int1; int2},idx);
    end

    function [f,df,ddf,rd] = evaluateFreqs(w)
        [f,df,ddf]  = arrayfun(ntfFn,w);
        rd          = (f - f_max)/f_max;
    end

    function findMaxes(ints,w0,f0,df0,ddf0)
        
        % Eliminate any potential starting points which will already by
        % considered stationary by fminunc/fmincon
        grad_norms  = arrayfun(@norm,df0);
        idx         = grad_norms <= tol;
        w0(idx)     = [];
        f0(idx)     = [];
        ints(idx)   = [];
        if isempty(w0) 
            return
        end
  
        % Resort starting values by highest function value first
        [~,idx]     = sort(f0,'descend');
        w0          = w0(idx);
        ints        = ints(idx);
          
        % Optimize from the k most promising points
        k           = min([opt_calls length(w0)]);
        [w,f]       = arrayfun(runOpt,w0(1:k));
        
        %[w,f]       = cellfun(optNtf,num2cell(w0(1:k)),ints(1:k));
        
        if plotting
            plotMaxes(w,f);
        end
    end

    function [w0,w_user] = getStartingGuesses()
        
        % Get any frequency guesses from the user, ensure as a column
        w_user      = opts.w0(:);
        
        if discrete
            w_user  = wrapToPi(w_user);
            w_def   = [0; pi];
            freqFn  = @angle;
        else
            w_def   = 0;
            freqFn  = @imag;
        end
          
        % Get the frequencies of the requested closest eigenvalues to the stability
        % boundary and most sensitive eigenvalues.
        
        n_w0_stab   = opts.n_w0_closest;
        n_w0_co     = opts.n_w0_sensitive;
        w_stab      = freqFn(d_stab(1:min([n_w0_stab length(d_stab)])));
        w_co        = freqFn(d_co(1:min([n_w0_co length(d_co)])));
        
        % Always include the default frequencies: 0 and possibly pi
        w0          = [w_def; w_stab; w_co; w_user];
          
        % If all matrices are real, problem is symmetric so we can exclude
        % negative frequencies.  Taking the absolute value may duplicate
        % positive ones but sorting via unique gets rid of them.
        if is_real
            w0      = abs(w0);
            w_user  = abs(w_user);
        end
        w0          = unique(w0);
        w_user      = unique(w_user);
    end

    function [is_stable,d] = isStable(d)
        if discrete
            stabBndFn   = @(z) abs(z) - 1;
        else
            stabBndFn   = @real;
        end
        % Compute the stability measure sm and check stability
        if linf_norm
            [sm,idx]    = sort(abs(stabBndFn(d)),'ascend');
            is_stable   = sm(1) > opts.stability_tol;
        else
            [sm,idx]    = sort(stabBndFn(d),'descend');
            is_stable   = sm(1) < -opts.stability_tol;
        end
        % Return the eigenvalues in order of least stable first
        d               = d(idx);
    end

    function opts = getIntervalOpts(fro_MN)
%         nEA                 = norm(toFull(A));
%         if ~isempty(E)
%             nEA             = cond(toFull(E))*nEA;
%         end
%         opts.ham_symp_tol   = sqrt(eps)*nEA;
        opts.discrete_time  = discrete;  
        opts.is_symmetric   = is_real;
        opts.ham_symp_tol   = eig_tol*fro_MN;
    end

    function plotIntervals(ints,ws,fs)       
        for j = 1:length(ints)
            lub     = ints{j};
            w       = ws(j);
            f       = fs(j);
            plot(lub,[f_max f_max],'ro:','LineWidth',2);
            plot([w w],[f_max f],'rx:','LineWidth',2);
        end
    end

    function plotInitialPoints(ws,fs,w_user)
        idx         = ismember(ws,w_user);
        if any(idx)
            f_user  = fs(idx);
        else
            w_user  = [];
            f_user  = [];
        end
        arrayfun(@(w,f) plot(w,f,'rsquare','LineWidth',2),ws,fs);
        arrayfun(@(w,f) plot(w,f,'bsquare','LineWidth',2),w_user,f_user);
    end

    function plotMaxes(ws,fs)
        arrayfun(@(w,f) plot(w,f,'ro','LineWidth',2),ws,fs);
    end
end

function o = getSpectrumOptions(opts)
    o.discrete_time         = opts.discrete_time;
    o.ignore_infinite       = false;
    o.ignore_unperturbable  = true;
end

function info = makeInfo(converged,iters,fn_evals,u,v)
    info = struct(  'converged',    converged,  ...
                    'iters',        iters,      ...
                    'fn_evals',     fn_evals,   ...
                    'u',            u,          ...
                    'v',            v           );
end

function x = replaceAt(x,x_new,idx)
    x = [x(1:idx-1); x_new; x(idx+1:end)];
end