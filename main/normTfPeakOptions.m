function opts = normTfPeakOptions(user_opts,varargin)
%   normTfPeakOptions:
%       Validate user options struct for normTfPeak.m.  If user_opts is []
%       or not provided, returned opts will be normTfPeak's default
%       parameters.
%
%   USAGE:
%       opts = normTfPeakOptions();
%       opts = normTfPeakOptions(user_opts);
%
%   INPUT:
%       user_opts   Struct of settable algorithm parameters.  No fields are 
%                   required, irrelevant fields are ignored, and user_opts 
%                   may be given as [].
%   
%   OUTPUT:
%       opts        Struct of all user-tunable normTfPeak parameters.  If a
%                   field is provided in user_opts, then the user's value
%                   is checked whether or not it is a valid value, and if
%                   so, it is set in opts.  Otherwise, an error is thrown.
%                   If a field is not provided in user_opts, opts will
%                   contain the field with its default value.
%
%   BASIC PARAMETERS 
%
%   .discrete_time              [logical | {false}]
%       False:  continuous-time system
%       True:   discrete-time system.
%
%   .w0                         [vector of real values | {[]}]                      
%       Initial frequencies to start optimization from.  If none are given,
%       optimization is started from 0 (continuous-time) or [0,pi]
%       (discrete-time).
%
%   .maxit                      [positive integer | {50}]
%       Maximum number of iterations per optimization run.  Optimization
%       generally converges within 10 iterations.  
%
%   .tol                        [positive finite real | {1e-14}]
%       The convergence tolerance for optimization.  Since optimization
%       converges quadratically or superlinearly, there is little reason to
%       set this significantly higher than machine precision.
%  
%   .use_hessian                [logical | {true}]                      
%       By default, any local optimization is done using Newton's method
%       when opts.optimization_calls > 0.  Setting this to false uses the
%       secant method, which only requires computing gradients, but
%       converges superlinearly instead of quadratically.
%
%   .solve_type                 [any value in [0,1,2] | {0}]                      
%       Sets how inverses of zE-A should be applied:
%           0: via upper triangular Hessenberg factorization 
%           1: via LU
%           2: via LU with permutations 
%       0 is generally recommended for small-scale systems, while 1 or 2 is
%       recommend for large-scale systems. 
%
%   .force_two_norm             [logical | {false}]                      
%       If B=C=I, D=0, and n=m=p, by default the norm of the transfer
%       function will be evaluated by computing the minimum singular value
%       of zE-A, as opposed to the norm of its inverse.  However, for some
%       very poorly scaled matrices, there can be numerical issues when
%       computing the smallest singular value using the builtin svd routine
%       numerical accuracy due to current limitations of the GESDD routine
%       in LAPACK, which is what svd generally uses.  In this case, one can
%       set this option to true to instead use norm(inv(zE-A)).
%
%   .suppress_warnings          [logical | {true}] 
%       By default, normTfPeak turns off all warnings while it is running
%       and restores the warning state when it terminates.  The reason is
%       that the LUs and backsolves used in evaluating the norm of the
%       transfer function may throw warnings, but as this code is already
%       designed to handle the causes of these warnings, there is no need
%       to ever see them on the console.
%
%   .plotting                   [logical | {false}] 
%       Enable plotting of the initial points opts.w0 and the computed
%       maximizers to the current figure.  Hold on should be called first.
%
%   See also normTfPeak.
%
%
%   For comments/bug reports, please visit the ROSTAPACK GitLab webpage:
%   https://gitlab.com/timmitchell/ROSTAPACK
%
%   normTfPeakOptions.m introduced in ROSTAPACK Version 3.0.
%
% =========================================================================
% |  ROSTAPACK: RObust STAbility PACKage                                  |
% |  Copyright (C) 2014-2021 Tim Mitchell                                 |
% |                                                                       |
% |  This file is part of ROSTAPACK                                       |
% |                                                                       |
% |  ROSTAPACK is free software: you can redistribute it and/or modify    |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  ROSTAPACK is distributed in the hope that it will be useful,         |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/agpl.html>.                             |
% =========================================================================

    persistent validator;
    
    if isempty(validator)
        validator   = optionValidator('normTfPeakOptions',getDefaults());
    end
    
    if nargin < 1 || isempty(user_opts)
        opts = validator.getDefaultOpts();
        return
    end
 
    validator.setUserOpts(user_opts);
    
    try
        % System options
        validator.setLogical('discrete_time');
        
        % Starting options
        if validator.isSpecified('w0')
            validator.setVector('w0');
            validator.setFiniteValued('w0');
            validator.setRealValued('w0');
        end
               
        % Termination options
        validator.setIntegerPositive('maxit');
        validator.setRealInIntervalOO('tol',0,inf);
        
        % Performance options
        validator.setLogical('use_hessian');
        validator.setIntegerInRange('solve_type',0,2);
        validator.setLogical('force_two_norm');
        
        % Plotting / output options
        validator.setLogical('suppress_warnings');
        validator.setLogical('plotting');   
    catch err
        err.throwAsCaller();
    end
   
    opts = validator.getValidatedOpts(); 
end

function default_opts = getDefaults()
    default_opts = struct(                              ...
        'discrete_time',            false,              ...
        'w0',                       [],                 ...
        'maxit',                    50,                ...
        'tol',                      1e-14,              ...
        'use_hessian',              true,               ...
        'solve_type',               0,                  ...
        'force_two_norm',           false,              ...
        'suppress_warnings',        true,               ...
        'plotting',                 false               );
end