function opts = normTfMaxPeakOptions(user_opts,varargin)
%   normTfMaxPeakOptions:
%       Validate user options struct for normTfMaxPeak.m.  If user_opts is
%       [] or not provided, returned opts will be normTfMaxPeak's default
%       parameters.
%
%   USAGE:
%       opts = normTfMaxPeakOptions();
%       opts = normTfMaxPeakOptions(user_opts);
%
%   INPUT:
%       user_opts   Struct of settable algorithm parameters.  No fields are 
%                   required, irrelevant fields are ignored, and user_opts 
%                   may be given as [].
%   
%   OUTPUT:
%       opts        Struct of all user-tunable normTfMaxPeak parameters.  
%                   If a field is provided in user_opts, then the user's
%                   value is checked whether or not it is a valid value,
%                   and if so, it is set in opts.  Otherwise, an error is
%                   thrown. If a field is not provided in user_opts, opts
%                   will contain the field with its default value.
%
%   BASIC PARAMETERS 
%
%   .discrete_time              [logical | {false}]
%       False:  continuous-time system
%       True:   discrete-time system.
%
%   .linf_norm                  [logical | {false}]
%       False:  Compute the H-infinity norm
%       True:   Compute the L-infinity norm
%
%   .stability_tol              [nonnegative finite real | {0}]
%       H-infinity norm: 
%           The controllable, observable eigenvalues z must be in the open
%           left half-plane (open unit disk) for a continuous-time
%           (discrete-time) system.  Making this tolerance positive further
%           restricts the stability region as follows: 
%           Continuous-time: 
%             max(real(z)) must be less than  -opts.stability_tol 
%           Discrete-time:
%             max(abs(z)) must be less than (1 - opts.stability_tol) 
%           for the system to be considered stable.
%       L-infinity norm:
%           The controllable, observable eigenvalues z must not be on the
%           imaginary axis (unit circle) for a continuous-time
%           (discrete-time) system.  Making this tolerance positive further
%           restricts the stability region as follows:
%           Continuous-time: 
%             min(abs(real(z))) must be greater than  opts.stability_tol 
%           Discrete-time:
%             min(abs(abs(z) - 1)) must be greater than opts.stability_tol
%           for the system to be considered stable.
%
%   .w0                         [vector of real values | {[]}]                      
%       Initial frequencies to evaluate the norm of the transfer function
%       at for starting the computation.  For continuous-time problems,
%       frequency zero is always tried, while zero and pi are always tried
%       for discrete-time problems.  
%
%   .n_w0_closest               [nonnegative integer | {10}]
%       In addition to opts.w0, frequencies corresponding to controllable
%       and observable system poles can also be used as initial guesses.
%       This sets how many such frequencies to try, where the frequencies
%       are taken from poles closest to the stability boundary first.
%
%   .n_w0_sensitive             [nonnegative integer | {10}]
%       In addition to opts.w0, frequencies corresponding to controllable
%       and observable system poles can also be used as initial guesses.
%       This sets how many such frequencies to try, where the frequencies 
%       are taken from poles with apparent high sensitivity first.  
%
%   .maxit                      [positive integer | {100}]
%       Maximum number of allowed iterations.  As the method has local
%       quadratic convergence, most problems only require a handful of
%       iterations.
%
%   .tol                        [positive finite real | {1e-14}]
%       The desired relative accuracy.  When opts.optimizaton_calls > 0,
%       there is little benefit to setting this significantly higher than
%       machine precision.
%   
%   .optimization_calls         [nonnegative integer | {1}]
%       Number of points to try local optimization from on each iteration.
%       Setting this to zero disables optimization, in which case, the
%       algorithm reduces to the Boyd-Balakrishnan-Bruinsma-Steinbuch
%       midpoint-based level-set iteration, which is slower.  This should
%       be set to at least 1.
%
%   .use_hessian                [logical | {true}]                      
%       By default, any local optimization is done using Newton's method
%       when opts.optimization_calls > 0.  Setting this to false uses the
%       secant method, which only requires computing gradients, but
%       converges superlinearly instead of quadratically.
%
%   .suppress_warnings          [logical | {true}] 
%       By default, normTfMaxPeak turns off all warnings while it is
%       running and restores the warning state when it terminates.  The
%       reason is that the LUs and backsolves used in evaluating the norm
%       of the transfer function may throw warnings, but as this code is
%       already designed to handle the causes of these warnings, there is
%       no need to ever see them on the console.
%
%   .plotting                   [logical | {false}] 
%       Enable plotting of the iterates of the algorithm to the current
%       figure.  Hold on should be called first. 
%
%   ADVANCED PARAMETERS: in general, these should be left at their defaults
%
%   .solve_type                 [any value in [0,1,2] | {0}]                      
%       Sets how inverses of zE-A should be applied:
%           0: via upper triangular Hessenberg factorization 
%           1: via LU
%           2: via LU with permutations 
%       0 is generally recommended for small-scale systems.
%
%   .force_two_norm             [logical | {false}]                      
%       If B=C=I, D=0, and n=m=p, by default the norm of the transfer
%       function will be evaluated by computing the minimum singular value
%       of zE-A, as opposed to the norm of its inverse.  However, for some
%       very poorly scaled matrices, there can be numerical issues when
%       computing the smallest singular value using the builtin svd routine
%       numerical accuracy due to current limitations of the GESDD routine
%       in LAPACK, which is what svd generally uses.  In this case, one can
%       set this option to true to instead use norm(inv(zE-A)).
%
%   .eig_tol                    [nonnegative real | {sqrt(eps)}]
%       Tolerance used to determine whether or not eigenvalues of the sHH
%       or sympletic matrix pencils should be considered imaginary or
%       unimodular, respectively.  While structure-preserving eigensolvers
%       mitigate against needing any tolerance (compared to using eig), a
%       non-zero value for this tolerance is still recommended.  This is
%       because even when using a structure-preserving eigensolver, e.g.,
%       two close imaginary eigenvalues could instead be returned as a pair
%       mirrored across the imaginary axis without breaking parity.
%
%   See also normTfMaxPeak.
%
%
%   For comments/bug reports, please visit the ROSTAPACK GitLab webpage:
%   https://gitlab.com/timmitchell/ROSTAPACK
%
%   normTfMaxPeakOptions.m introduced in ROSTAPACK Version 3.0.
%
% =========================================================================
% |  ROSTAPACK: RObust STAbility PACKage                                  |
% |  Copyright (C) 2014-2021 Tim Mitchell                                 |
% |                                                                       |
% |  This file is part of ROSTAPACK                                       |
% |                                                                       |
% |  ROSTAPACK is free software: you can redistribute it and/or modify    |
% |  it under the terms of the GNU Affero General Public License as       |
% |  published by the Free Software Foundation, either version 3 of       |
% |  the License, or (at your option) any later version.                  |
% |                                                                       |
% |  ROSTAPACK is distributed in the hope that it will be useful,         |
% |  but WITHOUT ANY WARRANTY; without even the implied warranty of       |
% |  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        |
% |  GNU Affero General Public License for more details.                  |
% |                                                                       |
% |  You should have received a copy of the GNU Affero General Public     |
% |  License along with this program.  If not, see                        |
% |  <http://www.gnu.org/licenses/agpl.html>.                             |
% =========================================================================

    persistent validator;
    
    if isempty(validator)
        validator = optionValidator('normTfMaxPeakOptions',getDefaults());
    end
    
    if nargin < 1 || isempty(user_opts)
        opts = validator.getDefaultOpts();
        return
    end
 
    validator.setUserOpts(user_opts);
    
    try
        % System options
        validator.setLogical('discrete_time');
        validator.setLogical('linf_norm');
        
        % Starting options
        validator.setRealInIntervalCO('stability_tol',0,inf);
        if validator.isSpecified('w0')
            validator.setVector('w0');
            validator.setFiniteValued('w0');
            validator.setRealValued('w0');
        end
        validator.setIntegerNonnegative('n_w0_closest');
        validator.setIntegerNonnegative('n_w0_sensitive');
               
        % Termination options
        validator.setIntegerPositive('maxit');
        validator.setRealInIntervalOO('tol',0,inf);
        
        % Performance options
        validator.setIntegerNonnegative('optimization_calls'); 
        validator.setLogical('use_hessian');
        validator.setIntegerInRange('solve_type',0,2);
        validator.setLogical('force_two_norm');
        validator.setRealNonnegative('eig_tol');
        
        % Plotting / output options
        validator.setLogical('suppress_warnings');
        validator.setLogical('plotting');   
    catch err
        err.throwAsCaller();
    end
   
    opts = validator.getValidatedOpts(); 
end

function default_opts = getDefaults()
    default_opts = struct(                              ...
        'discrete_time',            false,              ...
        'linf_norm',                false,              ...
        'stability_tol',            0,                  ...
        'w0',                       [],                 ...
        'n_w0_closest',             10,                 ...
        'n_w0_sensitive',           10,                 ...
        'maxit',                    100,                ...
        'tol',                      1e-14,              ...
        'optimization_calls',       1,                  ...
        'use_hessian',              true,               ...
        'solve_type',               0,                  ...
        'force_two_norm',           false,              ...
        'eig_tol',                  sqrt(eps),          ...
        'suppress_warnings',        true,               ...
        'plotting',                 false               );
end